import asyncio
import copy
import json
import logging
import os
import time

from authlib.integrations.requests_client import OAuth2Session
from fastapi import FastAPI, Depends, HTTPException, status, Path, Body, Query
from fastapi.middleware.cors import CORSMiddleware
from fastapi.security import HTTPBearer
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi_versionizer.versionizer import api_version, versionize
from jinja2 import Template
from starlette.config import Config
from starlette.requests import Request
from starlette.responses import JSONResponse

from {{ python_package_name }} import models
from {{ python_package_name }}.common import constants
from {{ python_package_name }}.common.exceptions import handle_exceptions, PermissionDenied
from {{ python_package_name }}.common.utility import convert_readme_to_html_docs, get_api_server_url_from_request, \
    get_base_url_from_request, get_url_for_app_from_request
from {{ python_package_name }}.rest import dependencies
from ska_src_permissions_api.client.permissions import PermissionsClient

config = Config('.env')

# Debug mode (runs unauthenticated)
#
DEBUG = True if config.get("DISABLE_AUTHENTICATION", default=None) == 'yes' else False

# Instantiate FastAPI() allowing CORS. Static mounts must be added later after the versionize() call.
#
app = FastAPI()
CORSMiddleware_params = {
    "allow_origins": ["*"],
    "allow_credentials": True,
    "allow_methods": ["*"],
    "allow_headers": ["*"]
}
app.add_middleware(CORSMiddleware, **CORSMiddleware_params)

# Set logging to use uvicorn logger.
#
logger = logging.getLogger("uvicorn")

# Get instances of constants.
#
IAM_CONSTANTS = constants.IAM(client_conf_url=config.get('IAM_CLIENT_CONF_URL'))

# Add HTTPBearer authz.
#
security = HTTPBearer()

# Instantiate an OAuth2 request session for the {{ python_package_name }} client.
#
API_IAM_CLIENT = OAuth2Session(config.get("API_IAM_CLIENT_ID"),
                               config.get("API_IAM_CLIENT_SECRET"),
                               scope=config.get("API_IAM_CLIENT_SCOPES", default=""))

# Get templates.
#
TEMPLATES = Jinja2Templates(directory="templates")

# Instantiate the permissions client.
#
PERMISSIONS = PermissionsClient(config.get('PERMISSIONS_API_URL'))
PERMISSIONS_SERVICE_NAME = config.get('PERMISSIONS_SERVICE_NAME')
PERMISSIONS_SERVICE_VERSION = config.get('PERMISSIONS_SERVICE_VERSION')

# Instantiate both common and permissions based dependencies
#
common_dependencies = dependencies.Common()
permission_dependencies = dependencies.Permissions(
    permissions=PERMISSIONS,
    permissions_service_name=PERMISSIONS_SERVICE_NAME,
    permissions_service_version=PERMISSIONS_SERVICE_VERSION
)

# Store service start time.
#
SERVICE_START_TIME = time.time()

# Keep track of number of managed requests.
#
REQUESTS_COUNTER = 0
REQUESTS_COUNTER_LOCK = asyncio.Lock()


# Routes
# ------
#
@api_version(1)
@app.get("/www/docs/oper",
         include_in_schema=False,
         dependencies=[Depends(common_dependencies.increment_request_counter)] if DEBUG else [
             Depends(common_dependencies.increment_request_counter)])
@handle_exceptions
async def oper_docs(request: Request) -> TEMPLATES.TemplateResponse:
    # Read and parse README.md, omitting excluded sections.
    if not DEBUG:
        readme_text_md = os.environ.get('README_MD', "")
    else:
        with open("../../../README.md") as f:
            readme_text_md = f.read()
    readme_text_html = convert_readme_to_html_docs(readme_text_md, exclude_sections=[
        "Development", "Deployment", "Prototype", "References"])

    openapi_schema = request.scope.get('app').openapi_schema
    openapi_schema_template = Template(json.dumps(openapi_schema))
    return TEMPLATES.TemplateResponse("docs.html", {
        "request": request,
        "base_url": get_base_url_from_request(request, config.get('API_SCHEME', default='http')),
        "page_title": "{{ api_name_hyphenated_and_capitalised }} API Operator Documentation",
        "openapi_schema": openapi_schema_template.render({
            "api_server_url": get_api_server_url_from_request(request, config.get('API_SCHEME', default='http'))
        }),
        "readme_text_md": readme_text_html,
        "version": "v{version}".format(version=os.environ.get('SERVICE_VERSION'))
    })


@api_version(1)
@app.get("/www/docs/user",
         include_in_schema=False,
         dependencies=[Depends(common_dependencies.increment_request_counter)] if DEBUG else [
             Depends(common_dependencies.increment_request_counter)])
@handle_exceptions
async def user_docs(request: Request) -> TEMPLATES.TemplateResponse:
    # Read and parse README.md, omitting excluded sections.
    if not DEBUG:
        readme_text_md = os.environ.get('README_MD', "")
    else:
        with open("../../../README.md") as f:
            readme_text_md = f.read()
    readme_text_html = convert_readme_to_html_docs(readme_text_md, exclude_sections=[
        "Authorisation", "Workflows", "Schemas", "Development", "Deployment", "Prototype", "References"])

    # Exclude unnecessary paths.
    paths_to_include = {
        '/ping': ['get'],
        '/health': ['get']
    }
    openapi_schema = copy.deepcopy(request.scope.get('app').openapi_schema)
    included_paths = {}
    for path, methods in openapi_schema.get('paths', {}).items():
        for method, attr in methods.items():
            if method in paths_to_include.get(path, []):
                if path not in included_paths:
                    included_paths[path] = {}
                included_paths[path][method] = attr
    openapi_schema.update({'paths': included_paths})

    openapi_schema_template = Template(json.dumps(openapi_schema))
    return TEMPLATES.TemplateResponse("docs.html", {
        "request": request,
        "base_url": get_base_url_from_request(request, config.get('API_SCHEME', default='http')),
        "page_title": "{{ api_name_hyphenated_and_capitalised }} API User Documentation",
        "openapi_schema": openapi_schema_template.render({
            "api_server_url": get_api_server_url_from_request(request, config.get('API_SCHEME', default='http'))
        }),
        "readme_text_md": readme_text_html,
        "version": "v{version}".format(version=os.environ.get('SERVICE_VERSION'))
    })


@api_version(1)
@app.get('/ping',
         responses={
             200: {"model": models.response.PingResponse}
         },
         tags=["Status"],
         summary="Check API status")
@handle_exceptions
async def ping(request: Request):
    """ Service aliveness. """
    return JSONResponse({
        'status': "UP",
        'version': os.environ.get('SERVICE_VERSION'),
    })


@api_version(1)
@app.get('/health',
         responses={
             200: {"model": models.response.HealthResponse},
             500: {"model": models.response.HealthResponse}
         },
         tags=["Status"],
         summary="Check API health")
@handle_exceptions
async def health(request: Request):
    """ Service health.

    This endpoint will return a 500 if any of the dependent services are down.
    """

    # Dependent services.
    #
    # Permissions API
    #
    permissions_api_response = PERMISSIONS.ping()

    # Set return code dependent on criteria e.g. dependent service statuses
    #
    healthy_criteria = [
        permissions_api_response.status_code == 200
    ]
    return JSONResponse(
        status_code=status.HTTP_200_OK if all(healthy_criteria) else status.HTTP_500_INTERNAL_SERVER_ERROR,
        content={
            'uptime': round(time.time() - SERVICE_START_TIME),
            'number_of_managed_requests': common_dependencies.requests_counter,
            'dependent_services': {
                'permissions-api': {
                    'status': "UP" if permissions_api_response.status_code == 200 else "DOWN",
                }
            }
        }
    )

# Versionise the API.
#
versions = versionize(
    app=app,
    prefix_format='/v{major}',
    docs_url=None,
    redoc_url=None
)
app.mount("/static", StaticFiles(directory="static"), name="static")

# Customise openapi.json.
#
# - Add schema server, title and tags.
# - Add request code samples to routes.
# - Remove 422 responses.
#
for route in app.routes:
    if isinstance(route.app, FastAPI):              # find any FastAPI subapplications (e.g. /v1/, /v2/, ...)
        subapp = route.app
        subapp_base_path = '{}{}'.format(os.environ.get('API_ROOT_PATH', default=''), route.path)
        subapp.openapi()
        subapp.openapi_schema['servers'] = [{"url": subapp_base_path}]
        subapp.openapi_schema['info']['title'] = '{{ api_name_hyphenated_and_capitalised }} API Overview'
        subapp.openapi_schema['tags'] = [
            {"name": "Status", "description": "Operations describing the status of the API.", "x-tag-expanded": False},
        ]
        # add request code samples and strip out 422s
        for language in ['shell', 'python', 'go', 'js']:
            for path, methods in subapp.openapi_schema['paths'].items():
                path = path.strip('/')
                for method, attr in methods.items():
                    if attr.get('responses', {}).get('422'):
                        del attr.get('responses')['422']
                    method = method.strip('/')
                    sample_template_filename = "{}-{}-{}.j2".format(
                        language, path, method).replace('/', '-')
                    sample_template_path = os.path.join('request-code-samples', sample_template_filename)
                    if os.path.exists(sample_template_path):
                        with open(sample_template_path, 'r') as f:
                            sample_source_template = f.read()
                        code_samples = attr.get('x-code-samples', [])
                        code_samples.append({
                            'lang': language,
                            'source': str(sample_source_template)            # rendered later in route
                        })
                        attr['x-code-samples'] = code_samples
